//
//  signUpPageController.swift
//  ARDA
//
//  Created by Guneet on 2019-07-01.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
class signUpPageController: UIViewController,GIDSignInUIDelegate {


    override func viewDidLoad() {
        
        super.viewDidLoad()
        print("NEXT VIEW")

        if(Auth.auth().currentUser != nil) {
            print("User is signed in")
            Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { (Timer) in
                   self.performSegue(withIdentifier: "profileSetup", sender: nil)
                }
        }
        else {
            print("not signed in")
            self.googleSignInAction()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//           self.performSegue(withIdentifier: "profileSetup", sender: nil)
    }
    func googleSignInAction() {
        let googleSignInButton = GIDSignInButton()
        googleSignInButton.frame = CGRect(x: 58, y: 362, width: 270, height: 50)
        view.addSubview(googleSignInButton)
        GIDSignIn.sharedInstance().uiDelegate = self
    }
  
    

}
