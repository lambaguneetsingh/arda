//
//  profileSetupController.swift
//  ARDA
//
//  Created by Guneet on 2019-07-02.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import WSTagsField
class profileSetupController: UIViewController {

    // MARK: user profile data from google
    
    var name:String?
    var email:String?
    var profilePicture:String?
    var userId:String?
    let userCredential = user_credentialsService()
    @IBOutlet weak var userProfile: UIImageView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userSkills: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var userInfo: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userProfile.layer.cornerRadius = self.userProfile.frame.size.height/2
        self.saveButton.layer.cornerRadius = self.saveButton.layer.frame.size.height/2
        loadDataintoController()
        placeholderForUSerInfo()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(screenTouchGesture))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
}
    
    
    
    
   func placeholderForUSerInfo() {
    if(self.userInfo.text == "")
    {
        self.userInfo.textColor = UIColor.lightGray
        self.userInfo.text = "Tell us abour yourself"
    }
    else if(self.userInfo.text != "")
    {
        self.userInfo.textColor = UIColor.black
    }
    if(self.userInfo.text == "Tell us abour yourself")
    {
        self.userInfo.textColor = UIColor.lightGray
    }
}

    @objc func screenTouchGesture() {
        self.placeholderForUSerInfo()
        view.endEditing(true)
}
 
    func loadDataintoController() {
        //  MARK: Get data from google account of current user.
            let user = Auth.auth().currentUser
            if let user = user {
            let uids = user.uid
            let names = user.displayName
            let emails = user.email
            let photURLs = user.photoURL
            if(names == "")
            {
                self.userName.placeholder = "No name present"
            }
            else {
                self.name = names!
                self.userName.text = names!
                self.userId = uids
            }
            if(self.email == "")
            {
                self.userEmail.placeholder = "No email present"
            
            }
            else {
                
                self.email = emails!
                self.userEmail.text = emails!
            }
            if(photURLs == nil)
            {
                self.profilePicture = nil
            }
            else {
            do {
            let data = try Data(contentsOf: photURLs!)
            self.userProfile.image = UIImage(data: data)
                self.profilePicture = String(describing: photURLs!)
            }
            catch {
            print("Error")
            }
            }
        }
        else {
            print("error occured")
        }
           
}
    
    
    
        @IBAction func saveButton(_ sender: Any) { 
        self.userCredential.saveDataToFirebase(uid: self.userId!, name: self.name!, email: self.email!, photourl: self.profilePicture!,skills: self.userSkills.text!,about: self.userInfo.text!)
            performSegue(withIdentifier: "homePage", sender: nil)
    }
    



}
